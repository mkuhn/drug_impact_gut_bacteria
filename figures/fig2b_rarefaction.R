#!/usr/bin/env Rscript

pacman::p_load(tidyverse, here)

source(here("processing_config.R"))

df_hit_counts <- read_tsv(paste0(output_data_path, "rarefaction.tsv"), col_types = cols())

df_hit_counts %>% write_tsv("figure_source_data/2b.tsv")

ggplot(df_hit_counts, aes(N, q50, color = drugs)) +
  geom_ribbon(aes(ymin = q25, ymax = q75, fill = drugs), size=0, alpha = 0.25, color="white") +
  geom_line() +
  xlab("Number of sampled strains") +
  ylab("Number of drugs with anticommensal activity") +
  theme_minimal() +
  scale_x_continuous(expand=c(0,0), limits = c(0, NA)) +
  geom_text(data = df_hit_counts %>% filter(N==max(N)), aes(label = drugs), color = "black", hjust = 1, vjust = -0.5, size = 2.5) +
  scale_fill_manual(values = c("#888888", "#66c2a5", "#fc8d62")) +
  scale_color_manual(values = c("#888888", "#66c2a5", "#fc8d62")) +
  theme(legend.position = "none",
        legend.title = element_text(size = 7),
        legend.text = element_text(size = 6),
        legend.margin = margin(1),
        legend.key.height = unit(0.75, "line"),
        axis.title = element_text(size = 7),
        axis.text = element_text(size = 6),
        plot.title = element_text(face = "bold", size = 8)
  )

ggsave("figure_pdf/fig2b_rarefaction.pdf", width = 5, height = 6, units = "cm")
