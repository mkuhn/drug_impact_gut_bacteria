#!/usr/bin/env Rscript

suppressPackageStartupMessages({
  if (!require("pacman")) install.packages("pacman")
  pacman::p_load(tidyverse, broom, here)
})

source(here("processing_config.R"))

abx_all_pv <- read_tsv(paste0(output_data_path, "abx_all_pv.tsv"), col_types = cols())

abx_enr_pv <- read_tsv(paste0(output_data_path, "abx_enr_pv.tsv"), col_types = cols(
  concept = col_character(),
  name = col_character(),
  N_se_abx = col_integer(),
  N_se_non_abx = col_integer(),
  pv = col_double(),
  adj_pv = col_double()
))

freq <- read_tsv(paste0(output_data_path, "se_freq.tsv"), col_types = cols(
  CID_flat = col_character(),
  concept = col_character(),
  name = col_character(),
  placebo = col_logical(),
  median_lo = col_double(),
  median_hi = col_double(),
  freq_all = col_double(),
  freq_exact = col_double()
))

p2at <- read_tsv(paste0(output_data_path, "p2at.tsv"), col_types = cols(
  prestwick_ID = col_character(),
  chemical_name = col_character(),
  chemical_formula = col_character(),
  molecular_weight = col_double(),
  cas_number = col_character(),
  therapeutic_class = col_character(),
  therapeutic_effect = col_character(),
  lc_chemical_name = col_character(),
  CID = col_character(),
  CID_flat = col_character(),
  ATC = col_character(),
  target_species = col_character(),
  veterinary = col_logical(),
  human_use = col_logical()
))

p2freq <- p2at %>% select(prestwick_ID, CID_flat) %>% unique() %>% inner_join(freq, by="CID_flat")

combined_pv <- read_tsv(paste0(output_data_path, "combined_pv.tsv"), col_types = cols(
  NT_code = col_character(),
  prestwick_ID = col_character(),
  pv_comb_fdr_BH = col_double(),
  AUC = col_double(),
  hit = col_logical(),
  target_species = col_character(),
  veterinary = col_logical(),
  human_use = col_logical()
))

chemical_names <- p2at %>% select(prestwick_ID, chemical_name) %>% unique()

combined_pv <- combined_pv %>% filter(human_use) %>% select(-veterinary, -human_use)
combined_pv <- combined_pv %>% left_join(chemical_names, by="prestwick_ID")

combined_hits <- combined_pv %>% group_by(target_species, prestwick_ID, chemical_name) %>% summarise(n_hit = sum(hit)) %>% ungroup()

combined_hits_freq <- combined_hits %>% inner_join(p2freq, by="prestwick_ID")
combined_hits_freq$abx_se <- combined_hits_freq$concept %in% abx_enr_pv$concept

combined_hits_freq$hit <- combined_hits_freq$n_hit > 0
combined_hits_freq$no_hit <- combined_hits_freq$n_hit == 0

combined_hits_freq$target_species_b <- ifelse(combined_hits_freq$target_species == "bacteria", "anti-bacterials",
                                            ifelse(combined_hits_freq$target_species == "human", "human-targeted drugs", "other anti-infectives"))

tp <- combined_hits_freq %>% filter(target_species == "human", !is.na(freq_exact))
tp <- tp %>% mutate(placebo = ifelse(placebo, "Placebo control", "Drug treatment"))

### PVALUES FOR PAPER
pvalues <- tp %>% group_by(abx_se, placebo) %>% do({
  d <- data.frame(wilcox.test(.$freq_exact[.$hit], .$freq_exact[!.$hit], alternative = "t") %>% tidy() )
  d$median_non_hit <- median(.$freq_exact[!.$hit])
  d$median_hit <- median(.$freq_exact[.$hit])
  d
})

tp$abx_se_ <- ifelse(tp$abx_se, "Antibiotics-related side effects", "Other side effects")

tc <- tp %>% group_by(abx_se, placebo, hit) %>%
  mutate( cdf = ecdf(freq_exact)(freq_exact) ) %>%
  select(abx_se, abx_se_, placebo, hit, freq_exact, cdf) %>% unique() %>%
  arrange(freq_exact) %>%
  do( bind_rows(.[1,] %>% mutate(cdf = 0), .) )


counts <- tp %>% group_by(abx_se, abx_se_, placebo, hit) %>% summarise(n=n())
counts <- counts %>% mutate(placebo_ = placebo != "Drug treatment",
                  label = paste0(ifelse(!hit, "\n", ""), "N = ", n, ifelse(!placebo_ & abx_se & hit, " drug-side effect pairs", "")),
                  x = ifelse(placebo_, 0.03, 0.15),
                  y = ifelse(placebo_, 0.24, 0.45))

medians <- tc %>% group_by(abx_se, placebo, hit) %>% mutate(delta = abs(cdf-0.5)) %>%
  filter(delta == min(delta)) %>% filter(freq_exact == min(freq_exact)) %>% mutate(cdf = 0.5)

pvlabel <- medians %>% ungroup() %>% filter(!hit) %>% inner_join(pvalues, by = c("abx_se", "placebo")) %>%
  mutate(label = sprintf(" p = %.2g ", p.value)) %>% select(abx_se_, placebo, freq_exact, label) %>%
  mutate(hjust = ifelse(placebo == "Drug treatment", "right", "left"),
         x = freq_exact,
         xt = ifelse(placebo == "Drug treatment", x + 0.015, x - 0.004)
         )

ggplot(tc, aes(freq_exact, 1-cdf, color = hit, linetype = placebo)) +
  geom_text(data=counts, aes(x=x, y=y, label=label), hjust="left", vjust="top", lineheight = 1, show.legend = F) +
  geom_segment(data = pvlabel, aes(x = x, xend = xt, y = 0.5, yend = 0.5), inherit.aes = F) +
  geom_text(data = pvlabel, aes(x = xt, y = 0.5, hjust = hjust, label = label), inherit.aes = F) +
  geom_step() +
  geom_point(data = medians, show.legend = F, shape = 15) +
  facet_wrap(~abx_se_, ncol = 1) +
  xlab("Incidence rate of side effect in patients") +
  ylab("Cumulative distribution") +
  coord_cartesian(xlim = c(0, 0.15)) +
  scale_x_reverse() +
  scale_color_manual(values = c(dbcolors$no_hit_alone, dbcolors$ht_hit),
                     breaks = c(T, F),
                     labels = c("with anticommensal activity", "not active"),
                     name = "Human-targeted drug:") +
  scale_linetype_manual( values = c("solid", "longdash"), name = "Clinical trial data:") +
  theme_minimal() +
  theme(strip.background = element_rect(fill="lightgrey", size = 0),
        panel.grid.minor.y = element_blank(), legend.position = "bottom",
        # legend.position = c(1,0), legend.justification = c(1, 0),
        legend.box.just = "left",
        legend.direction = "vertical")


tc %>% ungroup() %>% filter(abx_se) %>% select(-abx_se) %>%
  rename(indicidence_rate = freq_exact) %>%
  write_tsv("figure_source_data/3b.tsv")

ggplot(tc %>% filter(abx_se), aes(freq_exact, 1-cdf)) +
  geom_step(aes(color = hit, linetype = placebo)) +
  xlab("Incidence rate of ABX-related side effects in patients") +
  scale_x_reverse() +
  scale_y_continuous(name = "Cumulative distribution of drug-side effect pairs", limits = c(-0.01, 1), expand = c(0,0)) +
  coord_cartesian(xlim = c(0, 0.15)) +
  scale_linetype_manual( values = c("solid", "longdash"), name = "Clinical trial data:") +
  scale_color_manual(values = c(dbcolors$no_hit_alone, dbcolors$ht_hit),
                     breaks = c(T, F),
                     labels = c("with anticommensal activity", "inactive"),
                     name = "Human-targeted drug:") +
  theme_minimal() +
  theme(strip.background = element_rect(fill="lightgrey", size = 0),
        panel.grid.minor.y = element_blank(),
        legend.position = c(0,1), legend.justification = c(0, 1),
        legend.box.just = "left",
        legend.direction = "vertical",
        legend.title = element_text(size = 6),
        legend.text = element_text(size = 5),
        legend.margin = margin(1),
        legend.key.height = unit(0.4, "line"),
        axis.title = element_text(size = 6),
        axis.text = element_text(size = 5),
        plot.title = element_text(face = "bold", size = 7)
      )

ggsave("figure_pdf/fig3b.pdf", width = 6, height = 5, units = "cm")
