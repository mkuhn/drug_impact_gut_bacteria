#!/usr/bin/env Rscript

if (!require("pacman")) install.packages("pacman")
pacman::p_load(tidyverse, here, forcats, scales, stringi)

source(here("processing_config.R"))

oe_full <- read_tsv(paste0(output_data_path, "ecoli_overexpression_genes.tsv"), col_types = cols())
oe_full <- oe_full %>% mutate(q_min = pmin(q_low, q_high))

oe_full <- oe_full %>% left_join(read_tsv(here("annotations/ecoli_gene_function.tsv"), col_types = cols()), by="gene")

oe_filtered <- oe_full %>% filter(q_high < 0.1, cs > 6)

# oe_filtered %>% group_by(q_high < 0.01) %>% count()

drugs <- oe_filtered %>% group_by(drug) %>% summarise(n_gene = n(), cs = max(cs), cs_sum = sum(abs(cs))) %>%  ungroup() %>%
  arrange(drug) %>%
  ungroup %>% mutate(drug = fct_shift(factor(drug, levels = sort(as.character(drug))), 1), y = as.integer(drug))

drug_levels <- levels(drugs$drug)

genes <- oe_filtered %>% group_by(gene, Protein) %>% summarise(n_drug = n(), max_cs = max(cs)) %>% arrange(-max_cs) %>%
  ungroup %>% mutate(gene = fct_inorder(gene), y = as.integer(gene))

oe_filtered <- oe_filtered %>% mutate(drug = factor(drug, levels = drug_levels), gene = factor(gene, levels = genes$gene),
              y1 = as.integer(drug), y2 = as.integer(gene))

gene_levels <- rev(unique((oe_filtered %>% arrange(drug, -cs))$gene))

fsqrtext <- function(x) sign(x)*sqrt(abs(x))
isqrtext <- function(y) y*abs(y)
sqrtext_trans = function() trans_new("sqrtext", fsqrtext, isqrtext)

mlog10_trans = function() trans_new("mlog10", function(x) -log10(x), function(y) 10**(-y))

oe <- oe_full %>% filter(gene %in% genes$gene, drug %in% drugs$drug)
oe <- oe %>% mutate(drug = factor(drug, levels = drug_levels), gene = factor(gene, levels = gene_levels))

oe <- oe %>% mutate(
  BP = stri_replace_all_regex(Biological_Process, "(.{13}) ", "$1\n"),
  BP = fct_rev(fct_reorder(BP, cs, max)),
  BP2 = stri_replace_all_regex(Biological_Process2, "(.{13}) ", "$1\n"),
  BP2 = fct_rev(fct_reorder(BP2, cs, max)),
  batch = fct_collapse(drug, metformin = c("Metformin", "Control")),
  batch = ifelse(batch == "metformin", "metformin", "first_batch"))

oe <- oe %>% mutate(gene = recode(gene, "nfnB" = "nfsB", "yjcP" = "mdtP"))

oe_missing  <- expand.grid(drug = unique(oe$drug), gene = unique(oe$gene)) %>% anti_join(oe, by = c("drug", "gene")) %>%
  left_join(oe %>% select(gene, BP, BP2) %>% unique(), by="gene") %>%
  left_join(oe %>% select(drug, batch) %>% unique(), by="drug")



xy <- bind_rows(oe, oe_missing)

xy <- xy %>%
  mutate(BP = fct_rev(BP), BP2 = fct_rev(BP2)) %>%
  arrange(BP, BP2, gene, batch) %>%
  mutate(gene = fct_inorder(gene),
         drug = fct_inorder(drug),
         x = as.integer(drug) + 0.5*as.integer(factor(batch))-1,
         y = as.integer(gene) + 0.5*as.integer(BP)-1)

X <- xy %>% select(x, drug) %>% unique()
Y <- xy %>% filter(x == max(x)) %>% select(x, y, gene, BP, BP2) %>% unique()

YY <- Y %>% group_by(BP) %>%
  mutate(has_subgroups = length(unique(BP2))>1) %>%
  group_by(BP2) %>%
  summarise( ymin = min(y), ymax = max(y), y = mean(y), xl = x[1] + 1 + 1.5 * has_subgroups[1], x = xl + 0.2 * (n()>1)-0.05)

YY2 <- Y %>% anti_join(YY, by=c("BP"="BP2")) %>% group_by(BP) %>%
  summarise( ymin = min(y), ymax = max(y), y = mean(y), xl = x[1] + 1, x = xl + 0.1 * (n()>1))

known_genes <- stri_split_fixed("tolC, mdtP, rarD, mdtK, emrE, rob, rrmA, nfsA, nfsB, azoR", ", ")[[1]]

Y <- Y %>% mutate(face = ifelse(gene %in% known_genes, "bold.italic", "italic"))

xy %>% select(drug, ECK, gene, Protein, cs, p_high, q_high) %>% rename(pvalue = p_high, qvalue = q_high) %>%
  write_tsv("figure_source_data/4b.tsv")

ggplot(xy, aes(x, y, fill = cs)) + geom_rect(aes(xmin = x - 0.5, xmax = x + 0.5, ymin = y - 0.5, ymax = y + 0.5)) +
  geom_point(data = xy %>% filter(q_min < 0.1, cs > 6 | cs < -6), aes(size=q_min)) +
  geom_text(data = YY, aes(x = x, y = y, label = BP2), inherit.aes = F, hjust = "left", size = 2, lineheight = 0.9) +
  geom_text(data = YY2, aes(x = x, y = y, label = BP), inherit.aes = F, hjust = "center", vjust = "top", size = 2, angle = 90) +
  geom_text(data = Y, aes(x = -0.5, y = y, label = gene, fontface = face), inherit.aes = F, hjust = "right", size = 2) +
  geom_segment(data = bind_rows(YY, YY2) %>% filter(ymin != ymax),
               aes(x = xl, xend = xl, y = ymin - 0.5, yend = ymax + 0.5), inherit.aes = F, color = "grey") +
  scale_fill_gradient2(trans="sqrtext", breaks = c(-5, 0, 5, 20, 50, 100, 200), na.value = "#dddddd", name = "Growth difference\n(normalized)") +
  scale_size(trans="mlog10", breaks = c(1e-1, 1e-10, 1e-30), limits = c(1e-1, min(oe$q_min)), range = c(0.5,3), name = "q-value") +
  scale_x_continuous(position = "top", name = "", breaks = X$x, labels = X$drug, limits = c(-2.5, 18.5), expand = c(0,0) ) +
  scale_y_continuous(name = "Overexpressed gene in genome-wide screen", breaks = Y$y, labels = Y$gene, expand = c(0,0.1)) +
  # geom_vline(xintercept = length(drug_levels)+0.8, size = 2, color = "lightgrey") +
  theme_minimal() + coord_fixed() +
  theme(strip.text.y = element_text(angle=0, hjust = 0),
        panel.grid = element_blank(),
        axis.text.x.top = element_text(angle=45, hjust=0, size=6), legend.position = "bottom", legend.box = "vertical",
        axis.text.y = element_blank(),
        axis.title.y = element_text(size=7, margin = margin(0, 0, 0, 0, unit = "cm")),
        strip.text.x = element_blank(),
        legend.key.height = unit(0.75, "line"),
        legend.margin = margin(0,3,0,1,unit="line"),
        legend.title = element_text(size=7),
        legend.text = element_text(size=6))

ggsave("figure_pdf/fig4b_overexpression.pdf", width = 7, height = 12, units = "cm")
