#!/usr/bin/env Rscript

suppressPackageStartupMessages({
  if (!require("pacman")) install.packages("pacman")
  pacman::p_load(tidyverse, here, stringi, forcats, ggstance, broom)
})

source(here("processing_config.R"))

combined_hits <- read_tsv(paste0(output_data_path, "combined_hits.tsv"))

ddd <- read_tsv("annotations/drugbank_elimination_processed.tsv")
ddd <- ddd %>% left_join(combined_hits, by = "prestwick_ID") %>% filter(target_species == "human")

ddd %>% filter(!is.na(colon_conc_uM)) %>% group_by(hit) %>% summarise(n(), median(colon_conc_uM, na.rm=T), sum(colon_conc_uM < 20))
ddd %>% filter(!is.na(colon_conc_uM)) %>% summarise(n(), median(colon_conc_uM, na.rm=T))

ddd %>% filter(hit, colon_conc_uM < 20) %>% pull(chemical_name) %>% paste(collapse = ", ")

# ggplot(ddd, aes(colon_conc_uM, color=hit)) + geom_density() + scale_x_log10()


ddd_conc <- ddd %>% select(prestwick_ID, hit, colon_conc_uM, plasma_conc_uM, intestine_conc_uM) %>% unique() %>%
  gather(key = "kind", value = "conc", colon_conc_uM, plasma_conc_uM, intestine_conc_uM)

ddd_conc <- ddd_conc %>%
  mutate(kind_ = recode_factor(kind, intestine_conc_uM = "Estimated small intestine concentration",
                               plasma_conc_uM = "Plasma concentration",
                               colon_conc_uM = "Estimated colon concentration"))
ddd_conc <- ddd_conc %>% mutate(kind = fct_rev(kind))

ddd_fraction <- ddd %>% select(prestwick_ID, hit, fraction_feces, fraction_urine) %>%
  gather(key = "kind", value = "fraction", fraction_feces, fraction_urine)

ddd_fraction <- ddd_fraction %>% mutate(kind = recode(kind, fraction_feces = "Fecal excretion", fraction_urine = "Urinary excretion"))
ddd_fraction <- ddd_fraction %>% mutate(kind = fct_rev(kind))
ddd_fraction_m <- ddd_fraction %>% group_by(kind) %>% summarise(fraction = median(fraction, na.rm=T))

thm <- theme_minimal() + theme(strip.background = element_rect(fill="lightgrey", color="lightgrey"), panel.grid.minor = element_blank(),
                               axis.text = element_text(size = 6),  axis.title = element_text(size = 7), plot.title = element_text(size=8),
                               strip.text = element_text(size=8),
                               legend.position = "none")

plotConc <- function(ddd_conc) {
  lbl_conc <- ddd_conc %>% group_by(hit) %>% filter(!is.na(conc)) %>%
    summarise(label = paste0(paste0(rep("\n", 1+2*hit[1]), collapse = ""), "N = ", n()))

  ddd_conc_m <- ddd_conc %>% group_by(kind) %>% summarise(conc = median(conc, na.rm=T))

  ggplot(ddd_conc, aes(conc, fill=hit)) + geom_histogram(boundary = 1, binwidth = 0.5) + scale_x_log10(breaks = 10**seq(-4, 4, 2)) +
    geom_segment(x = log10(20), xend = log10(20), y = 0, yend = Inf, inherit.aes = F) +
    geom_text(data = lbl_conc, aes(label = label, color = hit), x = Inf, y = Inf, hjust = "inward", vjust = "center", size = 2) +
    geom_segment(data = ddd_conc_m, aes(x = conc, xend = conc, y= 0, yend = Inf), linetype = "dashed", inherit.aes = F) +
    geom_text(data = ddd_conc_m, aes(x = conc, label = round(conc, 1)), y = 0, vjust = "outward", size = 2, inherit.aes = F) +
    scale_y_continuous(expand = c(0.06, 0) ) +
    scale_fill_manual(values = c(dbcolors$no_hit, dbcolors$ht_hit)) +
    scale_color_manual(values = c(dbcolors$no_hit, dbcolors$ht_hit)) +
    xlab("Concentration (µM)") + ylab("Count") + thm

  ggsave(paste0("figure_pdf/figED3_concentrations_", ddd_conc$kind[1],".pdf"), width = 6, height = 4, units = "cm")
  data.frame()
}

ddd_conc %>% group_by(kind) %>% do(plotConc(.))

ddd_conc %>% group_by(kind) %>% filter(!is.na(conc)) %>% summarise(sum(conc<20)/n())


lbl_fraction <- ddd_fraction %>% group_by(hit, kind) %>% filter(!is.na(fraction)) %>% summarise(label = paste0(paste0(rep("\n", 1+2*hit[1]), collapse = ""), "N = ", n()))

plotFractions <- function(f) {

  ff <- paste0(f, " excretion")

  ggplot(ddd_fraction %>% filter(kind == ff), aes(fraction, fill=hit)) + geom_histogram(boundary = 0, binwidth = 0.1) +
    geom_segment(data = ddd_fraction_m %>% filter(kind == ff), aes(x = fraction, xend = fraction, y= 0, yend = Inf), linetype = "dashed", inherit.aes = F) +
    geom_text(data = ddd_fraction_m %>% filter(kind == ff), aes(x = fraction, label = round(fraction, 2)), y = 0, vjust = "outward", size = 2, inherit.aes = F) +
    geom_text(data = lbl_fraction %>% filter(kind == ff), aes(label = label, color = hit), x = 1, y = Inf, hjust = "inward", vjust = "center", size = 2) +
    scale_fill_manual(values = c(dbcolors$no_hit, dbcolors$ht_hit)) +
    scale_color_manual(values = c(dbcolors$no_hit, dbcolors$ht_hit)) +
    scale_y_continuous(expand = c(0.06, 0) ) +
    xlab("Fraction") + ylab("Count") + thm

  ggsave(paste0("figure_pdf/figED3_fractions_", f,".pdf"), width = 6, height = 4, units = "cm")

}

plotFractions("Fecal")
plotFractions("Urinary")


ddd_m <- ddd %>% filter(!is.na(amount_mole)) %>% ungroup() %>% summarise(amount = 1e6 * median(amount_mole) )

lbl_amount <- ddd %>% group_by(hit) %>% filter(!is.na(amount_mole)) %>% summarise(label = paste0(paste0(rep("\n", 1+2*hit[1]), collapse = ""), "N = ", n()))

ggplot(ddd, aes(amount_mole*1e6, fill=hit)) + geom_histogram(boundary = 1, binwidth = 0.5) + scale_x_log10(breaks = 10**seq(-4, 4, 2)) +
  geom_segment(data = ddd_m, aes(x = amount, xend = amount, y= 0, yend = Inf), linetype = "dashed", inherit.aes = F) +
  geom_text(data = ddd_m, aes(x = amount, label = round(amount, 0)), y = 0, vjust = "outward", size = 2, inherit.aes = F) +
  geom_text(data = lbl_amount, aes(label = label, color = hit), x = Inf, y = Inf, hjust = "inward", vjust = "center", size = 2) +
  scale_fill_manual(values = c(dbcolors$no_hit, dbcolors$ht_hit)) +
  scale_color_manual(values = c(dbcolors$no_hit, dbcolors$ht_hit)) +
  scale_y_continuous(expand = c(0.06, 0) ) +
  xlab("Drug dose (µmol)") + ylab("Count") + thm

ggsave("figure_pdf/figED3_amount.pdf", width = 6, height = 4, units = "cm")

pv_amount <-
  wilcox.test(ddd %>% filter(!hit) %>% pull(amount_mole),
              ddd %>% filter(hit) %>% pull(amount_mole))$p.value %>%
  sprintf(" p = %.2g", .)

pv_conc <- ddd_conc %>% group_by(kind_) %>% do(
  {
    d <- .
    wilcox.test(d %>% filter(!hit) %>% pull(conc),
                d %>% filter(hit) %>% pull(conc)
    ) %>% tidy()
  }
) %>% mutate(
  label = sprintf(" p = %.2g", p.value)
) %>% select(kind_, p.value, label)

lbl_conc <- ddd_conc %>% group_by(kind_, hit) %>% filter(!is.na(conc)) %>%
  summarise(label = paste0(paste0(rep("\n", 1+2*hit[1]), collapse = ""), "N = ", n()))

ggplot(ddd_conc, aes(conc, hit, color=hit)) +
  geom_boxploth(outlier.size = 0.25) +
  scale_x_log10(breaks = 10**seq(-4, 4, 2)) +
  expand_limits(x = 1e5) +
  geom_vline(xintercept = 20) +
  geom_text(data = lbl_conc, aes(label = stri_trim(label), y = hit), x = Inf, hjust = "inward", vjust = "center", size = 2, color = "black") +
  geom_text(data = pv_conc, aes(label = label), y = 1.5, x = -4, hjust = "inward", size = 2, inherit.aes = F) +
  facet_wrap(~kind_, ncol = 1, scales = "free_y") +
  scale_fill_manual(values = c(dbcolors$no_hit, dbcolors$ht_hit)) +
  scale_color_manual(values = c(dbcolors$no_hit, dbcolors$ht_hit)) +
  xlab("Concentration (µM)") + ylab("") + thm +
  theme(axis.text.y = element_blank(), panel.grid.major.y = element_blank(), strip.background = element_blank())

ggsave("figure_pdf/fig2a_concentrations.pdf", width = 7, height = 6, units = "cm")
