all: figure_pdf/fig1a.pdf figure_pdf/fig1b_drugs_by_target_species.pdf figure_pdf/fig1c_abundance.pdf \
	figure_pdf/fig2a_concentrations.pdf figure_pdf/figED3_amount.pdf figure_pdf/fig2b_rarefaction.pdf \
	figure_pdf/fig3a.pdf figure_pdf/figED7a.pdf figure_pdf/fig3b.pdf figure_pdf/fig3c.pdf \
	figure_pdf/figED8e.pdf figure_pdf/fig4a_human_vs_abx_gram_tolC.pdf \
	figure_pdf/fig4b_overexpression.pdf figure_pdf/fig4b_heatmap.pdf \
	figure_pdf/fig4b_heatmap_no_legend.pdf figure_pdf/fig4b_tanimoto.pdf \
	figure_pdf/figED10a_heatmap1.pdf figure_pdf/figED10a_tanimoto.pdf figure_pdf/figED10b.pdf \
	figure_pdf/figED10c.tiff figure_pdf/figED12_chemprop.pdf \
	figure_pdf/figED2c_QC_replicate_correlations.pdf figure_pdf/figED2d_pvalue_distribution.pdf \
	figure_pdf/figED4b_metformin_concordance.pdf figure_pdf/figED5b_MICs_grouped.pdf \
	figure_pdf/figED5c_fdr_cutoff.pdf figure_pdf/figED7b.pdf figure_pdf/figED7c.pdf \
	figure_pdf/figED7d_Akkermansia_AAP.pdf figure_pdf/figED11a.pdf figure_pdf/figED11a_heatmap1.pdf \
	figure_pdf/figED11a_tanimoto.pdf figure_pdf/figED11b.tiff figure_pdf/figED9b_heatmap.pdf \
	figure_pdf/figED9b_heatmap_no_legend.pdf figure_pdf/figED9b_tanimoto.pdf figure_pdf/figED10.pdf \
	figure_pdf/figED9a_indications.pdf

figure_pdf/fig1b_drugs_by_target_species.pdf: figure_pdf/fig1a.pdf

figure_pdf/fig1c_abundance.pdf: figure_pdf/fig1a.pdf

figure_pdf/fig1a.pdf: annotations/species_annotations.tsv annotations/tax_info_screen_specI_clusters.txt output/combined_pv.tsv output/species_overview.tsv
	-mkdir -p $(@D)
	figures/fig1_hit_overview.R

figure_pdf/figED3_amount.pdf: figure_pdf/fig2a_concentrations.pdf

figure_pdf/fig2a_concentrations.pdf: annotations/drugbank_elimination_processed.tsv output/combined_hits.tsv
	-mkdir -p $(@D)
	figures/fig2a_concentrations.R

figure_pdf/fig2b_rarefaction.pdf: output/rarefaction.tsv
	-mkdir -p $(@D)
	figures/fig2b_rarefaction.R

figure_pdf/figED7a.pdf: figure_pdf/fig3a.pdf

figure_pdf/fig3a.pdf: output/LM17-29-MIC.tsv output/combined_pv.tsv output/prestwick_atc.tsv output/species_overview.tsv
	-mkdir -p $(@D)
	figures/fig3a_PPI.R

figure_pdf/fig3b.pdf: output/abx_all_pv.tsv output/abx_enr_pv.tsv output/combined_pv.tsv output/p2at.tsv output/se_freq.tsv
	-mkdir -p $(@D)
	figures/fig3b_abx_se_freq.R

figure_pdf/figED8e.pdf: figure_pdf/fig3c.pdf

figure_pdf/fig3c.pdf: annotations/drugbank_elimination_processed.tsv output/LM16-150-MIC.tsv output/combined_hits.tsv output/prestwick_atc.tsv output/prestwick_ddd.tsv output/se_all_pv.tsv output/se_candidates.tsv output/species_overview.tsv
	-mkdir -p $(@D)
	figures/fig3c_se_validation.R

figure_pdf/fig4a_human_vs_abx_gram_tolC.pdf: annotations/species_annotations.tsv output/combined_pv.tsv output/extra_pv.tsv output/species_overview.tsv
	-mkdir -p $(@D)
	figures/fig4a_odds_ratio.R

figure_pdf/fig4b_overexpression.pdf: output/ecoli_overexpression_genes.tsv
	-mkdir -p $(@D)
	figures/fig4b_ecoli_overexpression.R

figure_pdf/fig4b_heatmap_no_legend.pdf: figure_pdf/fig4b_heatmap.pdf

figure_pdf/fig4b_tanimoto.pdf: figure_pdf/fig4b_heatmap.pdf

figure_pdf/figED10a_heatmap1.pdf: figure_pdf/fig4b_heatmap.pdf

figure_pdf/figED10a_tanimoto.pdf: figure_pdf/fig4b_heatmap.pdf

figure_pdf/figED10b.pdf: figure_pdf/fig4b_heatmap.pdf

figure_pdf/figED10c.tiff: figure_pdf/fig4b_heatmap.pdf

figure_pdf/fig4b_heatmap.pdf: output/atc_tree output/combined_pv.tsv output/prestwick_atc.tsv output/species_overview.tsv output/tanimoto_matrix.rds
	-mkdir -p $(@D)
	figures/fig4b_hit_heatmap.R

figure_pdf/figED12_chemprop.pdf: annotations/species_annotations.tsv output/combined_pv.tsv output/compound_properties.tsv output/p2at.tsv
	-mkdir -p $(@D)
	figures/figED12_chemprop.R

figure_pdf/figED2d_pvalue_distribution.pdf: figure_pdf/figED2c_QC_replicate_correlations.pdf

figure_pdf/figED2c_QC_replicate_correlations.pdf: output/aucs_pv.tsv
	-mkdir -p $(@D)
	figures/figED2cd_QC.R

figure_pdf/figED4b_metformin_concordance.pdf: annotations/nm.4345-S4.xlsx annotations/species_annotations.tsv output/LM16-121_142-MIC.tsv
	-mkdir -p $(@D)
	figures/figED4b_metformin_concordance.R

figure_pdf/figED5b_MICs_grouped.pdf: annotations/drugbank_elimination_processed.tsv output/LM16-121_142-MIC.tsv output/LM16-121_142-concentrations.tsv output/LM17-29-MIC.tsv output/LM17-29-concentrations.tsv output/combined_pv.tsv output/compound_properties.tsv output/species_overview.tsv
	-mkdir -p $(@D)
	figures/figED5_mic.R

figure_pdf/figED5c_fdr_cutoff.pdf: output/combined_pv.tsv
	-mkdir -p $(@D)
	figures/figED5b_fdr_threshold.R

figure_pdf/figED7c.pdf: figure_pdf/figED7b.pdf

figure_pdf/figED7b.pdf: output/combined_pv.tsv output/prestwick_atc.tsv output/species_overview.tsv
	-mkdir -p $(@D)
	figures/figED7bc_cohort_falony.R

figure_pdf/figED7d_Akkermansia_AAP.pdf: output/atc_tree output/combined_pv.tsv output/p2at.tsv output/prestwick_atc.tsv output/species_overview.tsv
	-mkdir -p $(@D)
	figures/figED7d_akkermansia_antipsychotics.R

figure_pdf/figED11a_heatmap1.pdf: figure_pdf/figED11a.pdf

figure_pdf/figED11a_tanimoto.pdf: figure_pdf/figED11a.pdf

figure_pdf/figED11b.tiff: figure_pdf/figED11a.pdf

figure_pdf/figED9b_heatmap.pdf: figure_pdf/figED11a.pdf

figure_pdf/figED9b_heatmap_no_legend.pdf: figure_pdf/figED11a.pdf

figure_pdf/figED9b_tanimoto.pdf: figure_pdf/figED11a.pdf

figure_pdf/figED11a.pdf: output/atc_tree output/combined_pv.tsv output/prestwick_atc.tsv output/species_overview.tsv output/tanimoto_matrix.rds
	-mkdir -p $(@D)
	figures/figED9_hit_heatmap.R

figure_pdf/figED9a_indications.pdf: figure_pdf/figED10.pdf

figure_pdf/figED10.pdf: output/atc_tree output/combined_pv.tsv output/p2a1.tsv output/p2at.tsv output/prestwick_ddd.tsv
	-mkdir -p $(@D)
	figures/figED9a_indication_areas.R
